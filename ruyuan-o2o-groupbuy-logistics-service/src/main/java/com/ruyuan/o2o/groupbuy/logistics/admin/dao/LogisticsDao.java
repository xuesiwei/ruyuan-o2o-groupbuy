package com.ruyuan.o2o.groupbuy.logistics.admin.dao;

import com.ruyuan.o2o.groupbuy.logistics.model.LogisticsModel;
import org.springframework.stereotype.Repository;

/**
 * 物流配送服务DAO
 *
 * @author ming qian
 */
@Repository
public interface LogisticsDao {
    /**
     * 保存物流配送信息
     *
     * @param logisticsModel
     */
    void save(LogisticsModel logisticsModel);

}