package com.ruyuan.o2o.groupbuy.overage.admin.service;

import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.overage.admin.dao.OverageDao;
import com.ruyuan.o2o.groupbuy.overage.model.OverageModel;
import com.ruyuan.o2o.groupbuy.overage.service.OverageService;
import com.ruyuan.o2o.groupbuy.overage.vo.OverageVO;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 处理充值服务的增删改查service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = OverageService.class, cluster = "failfast", loadbalance = "roundrobin")
public class OverageServiceImpl implements OverageService {

    @Autowired
    private OverageDao overageDao;

    /**
     * 创建充值
     *
     * @param overageVO 充值vo
     */
    @Override
    public void save(OverageVO overageVO) {
        OverageModel overageModel = new OverageModel();
        BeanMapper.copy(overageVO, overageModel);
        overageModel.setCreateTime(TimeUtil.format(System.currentTimeMillis()));
        overageModel.setUpdateTime(TimeUtil.format(System.currentTimeMillis()));
        overageDao.save(overageModel);
    }

    /**
     * 查询余额
     *
     * @param userId  消费者用户id
     * @param storeId 门店id
     * @return 余额
     */
    @Override
    public Double findOverage(Integer userId, Integer storeId) {
        OverageModel overageModel = new OverageModel();
        overageModel.setUserId(userId);
        overageModel.setStoreId(storeId);
        return overageDao.findOverage(overageModel);
    }
}
