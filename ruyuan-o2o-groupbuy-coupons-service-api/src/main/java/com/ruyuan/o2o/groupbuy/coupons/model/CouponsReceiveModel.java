package com.ruyuan.o2o.groupbuy.coupons.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 用户领取优惠券实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class CouponsReceiveModel implements Serializable {
    private static final long serialVersionUID = -3893554566280810813L;

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 优惠券id
     */
    private Integer couponsId;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 优惠券状态 0 未使用 1 已使用 2 已过期
     */
    private Integer couponsStatus;

    /**
     * 领取时间
     */
    private String receiveTime;
}