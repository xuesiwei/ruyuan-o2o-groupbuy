package com.ruyuan.o2o.groupbuy.overage.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 充值服务实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class OverageModel implements Serializable {
    private static final long serialVersionUID = 647385098817721843L;

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 消费者用户id
     */
    private Integer userId;

    /**
     * 门店id
     */
    private Integer storeId;

    /**
     * 消费者在门店本次充值余额
     */
    private Double overageAdd;

    /**
     * 消费者在门店总余额
     */
    private Double overageTotal;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;
}