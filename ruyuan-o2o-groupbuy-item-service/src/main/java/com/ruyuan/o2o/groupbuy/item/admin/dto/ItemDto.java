package com.ruyuan.o2o.groupbuy.item.admin.dto;

import com.ruyuan.o2o.groupbuy.evaluate.model.EvaluateModel;
import com.ruyuan.o2o.groupbuy.item.model.ItemModel;
import com.ruyuan.o2o.groupbuy.promotions.model.PromotionsModel;
import com.ruyuan.o2o.groupbuy.store.model.StoreModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 封装用户端商品详情数据
 * 由商品、活动、门店、评价等信息组成
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class ItemDto {

    private ItemModel itemModel;

    private PromotionsModel promotionsModel;

    private StoreModel storeModel;

    private EvaluateModel evaluateModel;
}
