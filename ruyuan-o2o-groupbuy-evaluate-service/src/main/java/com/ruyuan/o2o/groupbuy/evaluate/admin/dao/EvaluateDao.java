package com.ruyuan.o2o.groupbuy.evaluate.admin.dao;

import com.ruyuan.o2o.groupbuy.evaluate.model.EvaluateModel;
import org.springframework.stereotype.Repository;

/**
 * 评价服务DAO
 *
 * @author ming qian
 */
@Repository
public interface EvaluateDao {

    /**
     * 保存评价
     *
     * @param evaluateModel 评价model
     */
    void save(EvaluateModel evaluateModel);

    /**
     * 根据商品id查询商品评价
     *
     * @param itemId 商品id
     * @return 评价model
     */
    EvaluateModel findByItemId(Integer itemId);

}