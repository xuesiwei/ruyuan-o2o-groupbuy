package com.ruyuan.o2o.groupbuy.address.admin.service;

import com.github.pagehelper.PageHelper;
import com.ruyuan.o2o.groupbuy.account.service.AccountService;
import com.ruyuan.o2o.groupbuy.address.admin.dao.AddressMappingDao;
import com.ruyuan.o2o.groupbuy.address.model.AddressMappingModel;
import com.ruyuan.o2o.groupbuy.address.service.AddressMappingService;
import com.ruyuan.o2o.groupbuy.address.vo.AddressMappingVO;
import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * 处理省市区映射服务的增删改查service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = AddressMappingService.class, cluster = "failfast", loadbalance = "roundrobin")
@Slf4j
public class AddressMappingServiceImpl implements AddressMappingService {

    @Autowired
    private AddressMappingDao addressMappingDao;

    /**
     * 创建地址映射
     *
     * @param addressMappingVO
     */
    @Override
    public Boolean save(AddressMappingVO addressMappingVO) {
        AddressMappingModel addressMappingModel = new AddressMappingModel();
        BeanMapper.copy(addressMappingVO, addressMappingModel);
        addressMappingDao.save(addressMappingModel);
        return Boolean.TRUE;
    }

    /**
     * 更新地址映射
     *
     * @param addressMappingVO
     */
    @Override
    public void update(AddressMappingVO addressMappingVO) {
        AddressMappingModel addressMappingModel = new AddressMappingModel();
        BeanMapper.copy(addressMappingVO, addressMappingModel);
        addressMappingDao.update(addressMappingModel);
    }

    /**
     * 分页查询地址映射
     *
     * @return
     */
    @Override
    public List<AddressMappingVO> listByPage(Integer pageNum, Integer pageSize) {
        List<AddressMappingVO> addressMappingVoS = new ArrayList<>();

        PageHelper.startPage(pageNum, pageSize);
        List<AddressMappingModel> addressMappingModels = addressMappingDao.listByPage();

        for (AddressMappingModel addressMappingModel : addressMappingModels) {
            AddressMappingVO addressMappingVO = new AddressMappingVO();
            BeanMapper.copy(addressMappingModel, addressMappingVO);
            addressMappingVoS.add(addressMappingVO);
        }

        return addressMappingVoS;
    }

    /**
     * 查询省名称
     *
     * @param provinceId 省id
     * @return
     */
    @Override
    public String findProvinceName(Integer provinceId) {
        return addressMappingDao.findProvinceName(provinceId);
    }

    /**
     * 查询市名称
     *
     * @param cityId 市id
     * @return
     */
    @Override
    public String findCityName(Integer cityId) {
        return addressMappingDao.findCityName(cityId);
    }

    /**
     * 查询区名称
     *
     * @param districtId 区id
     * @return
     */
    @Override
    public String findDistrictName(Integer districtId) {
        return addressMappingDao.findDistrictName(districtId);
    }

}
