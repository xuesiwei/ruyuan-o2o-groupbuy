package com.ruyuan.o2o.groupbuy.account.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 用户角色绑定关系实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class AccountRoleModel {
    /**
     * 主键id
     */
    private Integer id;
    /**
     * 用户id
     */
    private Integer adminId;
    /**
     * 角色id
     */
    private Integer roleId;
}
