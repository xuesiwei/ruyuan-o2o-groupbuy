package com.ruyuan.o2o.groupbuy.account.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 商户账号VO类
 *
 * @author ming qian
 */
@ApiModel(value = "商户账号VO类")
@Getter
@Setter
@ToString
public class AccountVO implements Serializable {
    private static final long serialVersionUID = -5367057286321830430L;
    /**
     * 管理用户id
     */
    @ApiModelProperty("管理用户id")
    private Integer adminId;

    /**
     * 管理用户名称
     */
    @ApiModelProperty("管理用户名称")
    private String adminName;

    /**
     * 手机号码
     */
    @ApiModelProperty("手机号码")
    private Integer mobile;

    /**
     * 密码
     */
    @ApiModelProperty("密码")
    private String passwd;

    /**
     * 角色id
     */
    @ApiModelProperty("角色id")
    private Integer roleId;

    /**
     * 最后登录时间
     */
    @ApiModelProperty("最后登录时间")
    private String lastLogin;

    /**
     * 最后登录ip
     */
    @ApiModelProperty("最后登录ip")
    private String lastIp;

    /**
     * 账号是否关闭 0 正常 1 关闭
     */
    @ApiModelProperty("账号是否关闭 0 正常 1 关闭")
    private Integer closed;

    /**
     * 账号创建时间
     */
    @ApiModelProperty("账号创建时间")
    private String createDate;
}
