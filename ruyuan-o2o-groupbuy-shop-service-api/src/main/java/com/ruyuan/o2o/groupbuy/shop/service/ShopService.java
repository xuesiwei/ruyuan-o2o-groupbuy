package com.ruyuan.o2o.groupbuy.shop.service;

import com.ruyuan.o2o.groupbuy.shop.vo.ShopStoreVO;

import java.util.List;

/**
 * 商户服务service组件接口
 *
 * @author ming qian
 */
public interface ShopService {
    /**
     * 创建门店
     *
     * @param shopStoreVO 商户操作门店vo
     * @return 创建结果
     */
    Boolean save(ShopStoreVO shopStoreVO);

    /**
     * 更新门店
     *
     * @param shopStoreVO 商户操作门店vo
     * @return 更新结果
     */
    Boolean update(ShopStoreVO shopStoreVO);

    /**
     * 下架门店
     *
     * @param storeId 门店id
     * @return 下架结果
     */
    Boolean offLine(Integer storeId);

    /**
     * 上架门店
     *
     * @param storeId 门店id
     * @return 上架结果
     */
    Boolean onLine(Integer storeId);

    /**
     * 分页查询门店
     *
     * @param pageNum  查询页数
     * @param pageSize 每次查询数
     * @return 查询列表
     */
    List<ShopStoreVO> listByPage(Integer pageNum, Integer pageSize);

    /**
     * 根据id查询门店
     *
     * @param storeId 门店id
     * @return 商户操作门店vo
     */
    ShopStoreVO findById(Integer storeId);

    /**
     * 删除门店
     *
     * @param storeId 门店id
     * @return 删除结果
     */
    Boolean delete(Integer storeId);
}
