package com.ruyuan.o2o.groupbuy.message.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 短信模板VO类
 *
 * @author ming qian
 */
@ApiModel(value = "短信模板VO类")
@Getter
@Setter
@ToString
public class MessageTemplateVO implements Serializable {
    private static final long serialVersionUID = 2310430390121853646L;

    /**
     * 短信id
     */
    @ApiModelProperty("短信id")
    private Integer messageId;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private String createTime;

    /**
     * 短信模板内容
     */
    @ApiModelProperty("短信模板内容")
    private String messageTemplate;

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    private Integer createOper;

    /**
     * 短信模板类型 0 提醒支付短信 1 订单支付完成短信 2 物流配送短信 3 门店核销券码短信 4 活动上新短信 5 优惠券过期提醒短信
     */
    @ApiModelProperty("短信模板类型 0 提醒支付短信 1 订单支付完成短信 2 物流配送短信 3 门店核销券码短信 4 活动上新短信 5 优惠券过期提醒短信")
    private Integer messageTemplateType;
}