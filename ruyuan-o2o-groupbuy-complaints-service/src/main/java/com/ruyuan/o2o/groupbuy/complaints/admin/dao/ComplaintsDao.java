package com.ruyuan.o2o.groupbuy.complaints.admin.dao;

import com.ruyuan.o2o.groupbuy.complaints.model.ComplaintsModel;
import org.springframework.stereotype.Repository;

/**
 * 投诉服务DAO
 *
 * @author ming qian
 */
@Repository
public interface ComplaintsDao {
    /**
     * 接收投诉
     *
     * @param complaintsModel
     */
    void save(ComplaintsModel complaintsModel);
}