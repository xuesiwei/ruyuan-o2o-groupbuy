package com.ruyuan.o2o.groupbuy.order.http;

import com.ruyuan.o2o.groupbuy.order.service.OrderService;
import com.ruyuan.o2o.groupbuy.order.vo.OrderBuyVO;
import com.ruyuan.o2o.groupbuy.record.service.RecordService;
import com.ruyuan.o2o.groupbuy.record.vo.RecordVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

/**
 * 用户商品详情页立即购买
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/order")
@Slf4j
@Api(tags = "用户端订单管理")
public class OrderHttpController {

    @Reference(version = "1.0.0", interfaceClass = OrderService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private OrderService orderService;

    /**
     * 立即购买
     *
     * @param orderBuyVO 前台表单
     * @return
     */
    @PostMapping("/buy")
    @ApiOperation("立即购买")
    public Boolean save(@RequestBody OrderBuyVO orderBuyVO) {
        //  用户需要先登录
        if (!orderBuyVO.getIsLogin()) {
            log.error("当前用户未登录,系统拦截跳转到用户注册/登录页面,保存消费者用户信息");
            return Boolean.FALSE;
        }
        //  封装订单数据
        orderService.save(orderBuyVO);
        log.info("创建订单完成");

        return Boolean.TRUE;
    }
}
