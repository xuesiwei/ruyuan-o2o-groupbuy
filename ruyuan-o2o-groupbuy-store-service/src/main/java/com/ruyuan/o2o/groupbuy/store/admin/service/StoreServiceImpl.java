package com.ruyuan.o2o.groupbuy.store.admin.service;

import com.github.pagehelper.PageHelper;
import com.ruyuan.o2o.groupbuy.account.service.AccountService;
import com.ruyuan.o2o.groupbuy.account.vo.AccountVO;
import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.store.admin.dao.StoreDao;
import com.ruyuan.o2o.groupbuy.store.model.StoreModel;
import com.ruyuan.o2o.groupbuy.store.service.StoreService;
import com.ruyuan.o2o.groupbuy.store.vo.StoreVO;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * 处理门店服务操作门店更新信息，查看门店信息的service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = StoreService.class, cluster = "failfast", loadbalance = "roundrobin")
public class StoreServiceImpl implements StoreService {

    @Autowired
    private StoreDao storeDao;

    /**
     * 更新门店
     *
     * @param storeVO
     */
    @Override
    public Boolean update(StoreVO storeVO) {
        StoreModel storeModel = new StoreModel();
        BeanMapper.copy(storeVO, storeModel);
        storeModel.setUpdateTime(TimeUtil.format(System.currentTimeMillis()));
        storeModel.setUpdateOper(storeVO.getStoreId());
        storeDao.update(storeModel);
        return Boolean.TRUE;
    }

    /**
     * 分页查询门店
     *
     * @return
     */
    @Override
    public List<StoreVO> listByPage(Integer pageNum, Integer pageSize) {
        List<StoreVO> storeVoS = new ArrayList<>();

        PageHelper.startPage(pageNum, pageSize);
        List<StoreModel> storeModels = storeDao.listByPage();

        for (StoreModel storeModel : storeModels) {
            StoreVO storeVO = new StoreVO();
            BeanMapper.copy(storeModel, storeVO);
            storeVoS.add(storeVO);
        }

        return storeVoS;
    }

    /**
     * 根据id查询门店
     *
     * @param storeId 门店id
     * @return
     */
    @Override
    public StoreVO findById(Integer storeId) {
        StoreModel storeModel = storeDao.findById(storeId);
        StoreVO storeVO = new StoreVO();
        BeanMapper.copy(storeModel, storeVO);
        return storeVO;
    }
}
