package com.ruyuan.o2o.groupbuy.code.admin.controller;

import com.ruyuan.o2o.groupbuy.code.service.CodeService;
import com.ruyuan.o2o.groupbuy.code.vo.CodeVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 券码管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/code")
@Slf4j
@Api(tags = "券码管理")
public class CodeController {

    @Reference(version = "1.0.0", interfaceClass = CodeService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private CodeService codeService;

    /**
     * 商户根据用户id查询券码
     *
     * @param userId 用户id
     * @param payId  支付id
     * @return
     */
    @PostMapping("/find")
    @ApiOperation("查询券码")
    public CodeVO page(Integer userId, Integer payId) {
        CodeVO codeVO = codeService.findById(userId, payId);
        return codeVO;
    }
}
